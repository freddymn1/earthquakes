package com.earthquakes.utils;

public enum InfoEnumUtil {

	INI_CONSULTA_SISMO_RANGOFECHA("001-INI CONTROLLER CONSULTA SISMOS CONTROLLER ",
			"Consulta Sismo segun rango de Fechas"),
	
	FIN_CONSULTA_SISMO_RANGOFECHA("002-FIN CONTROLLER CONSULTA SISMOS CONTROLLER ",
			"FIN Controller Consulta Sismo segun rango de Fechas"),

	INI_CONSULTA_SERVICE("003-INI SERVICE CONSULTA SISMOS CONTROLLER","INI Services Consulta Sismo segun rango de Fechas"),
	
	FIN_CONSULTA_SERVICE("004-FIN SERVICE CONSULTA SISMOS CONTROLLER","FIN Services Consulta Sismo segun rango de Fechas"),
	INI_CONSULTA_SERVICE_EARTHQUAKE("005-INI CONSULTA SERVICIO EARTHQUAKE","INI consulta servicio EARTHQUAKE"),
	FIN_CONSULTA_SERVICE_EARTHQUAKE("005-INI CONSULTA SERVICIO EARTHQUAKE","INI consulta servicio EARTHQUAKE"),
	
	
	INI_CONSULTA_SISMO_MAGNITUD("001-INI CONTROLLER CONSULTA MAGNITUD CONTROLLER ",
			"Consulta Magnitud Sismo"),
	
	FIN_CONSULTA_SISMO_MAGNITUD("002-FIN CONTROLLER CONSULTA MAGNITUD CONTROLLER ",
			"FIN Controller Consulta Magnitud Sismo");

	
	private final String code;
	private final String msg;

	InfoEnumUtil(String code, String msg) {
		this.code = code;
		this.msg = msg;
	}

	public String getCode() {
		return this.code;
	}

	public String getMsg() {
		return this.msg;
	}
}