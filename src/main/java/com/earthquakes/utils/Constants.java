package com.earthquakes.utils;

import java.io.Serializable;

public class Constants implements Serializable {

	private static final long serialVersionUID = 6354630435599385052L;

	public Constants() {
		throw new IllegalStateException("Utility class");
	}

	public static final String FORMATO_FECHA_SB = "dd/MM/yyyy";
	public static final String FORMATO_FECHA_BUK = "yyyy-MM-dd";
	public static final String ESTADO_FINALIZADO = "FINALIZADO";
	public static final String MOTIVO_INASISTENCIA = "INASISTENCIA";
	public static final String MOTIVO_PERMISO = "PERMISO";
	public static final int REGISTRO_FALLIDO_BUK = 500;
	public static final int ERROR_DATOS_ENVIADOS = 400;
	public static final int REGISTRO_CREADO_BUK = 201;
	public static final int SUCCESS = 200;
	public static final String EMPLEADOID_NOTFOUND = "EmployeeId no encontrado";
	public static final String REGISTRO_BUK_NOK = " No Registrado en Buk";
	public static final String REGISTRO_BUK_OK = "Registrado en Buk";
	public static final String ESTADO_ENVIO_BUK = "EnvioBuk";

}
