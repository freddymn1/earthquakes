package com.earthquakes.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Data
@Entity
@Getter
@Setter
@ToString
@Table(name = "SISMOS")

public class SismosEntity {

	
	@Id
	@Column(name = "mag" ,nullable = true)
	private double mag;
	
	@Column(name = "place" ,nullable = true)
	private String place;
	
	@Column(name = "tim" ,nullable = true)
	private long tim;
	
	@Column(name = "updated",nullable = true)
	private long updated;
	
	@Column(name = "tz",nullable = true)
	private int tz;
	
	@Column(name = "url",nullable = true)
	private String url;
	
	@Column(name = "detail",nullable = true)
	private String detail;
	
	@Column(name = "felt",nullable = true)
	private String felt;
	
	@Column(name = "cdi",nullable = true)
	private String cdi;
	
	@Column(name = "mmi",nullable = true)
	private String mmi;
	
	@Column(name = "alert",nullable = true)
	private String alert;
	
	@Column(name = "status",nullable = true)
	private String status;
	
	@Column(name = "tsunami",nullable = true)
	private int tsunami;
	
	@Column(name = "sig",nullable = true)
	private int sig;
	
	@Column(name = "net",nullable = true)
	private String net;
	
	@Column(name = "code",nullable = true)
	private String code;
	
	@Column(name = "ids",nullable = true)
	private String ids;
	
	@Column(name = "sources",nullable = true)
	private String sources;
	
	@Column(name = "types",nullable = true)
	private String types;
	
	@Column(name = "nst",nullable = true)
	private int nst;
	
	@Column(name = "dmin",nullable = true)
	private double dmin;
	
	@Column(name = "rms",nullable = true)
	private double rms;
	
	@Column(name = "gap",nullable = true)
	private String gap;
	
	@Column(name = "magType",nullable = true)
	private String magType;
	
	@Column(name = "type",nullable = true)
	private String type;
	
	@Column(name = "title",nullable = true)
	private String title;

}
