package com.earthquakes.controller;

import java.text.ParseException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.earthquakes.dto.EarthquakesMagnitudRequest;
import com.earthquakes.dto.EarthquakesRequest;
import com.earthquakes.dto.EarthquakesResponse;
import com.earthquakes.dto.GenericDtoResponse;
import com.earthquakes.service.EarthquakesMagnitudService;
import com.earthquakes.service.EarthquakesSismosService;
import com.earthquakes.utils.InfoEnumUtil;
import com.earthquakes.utils.LogMessageUtil;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * Clase para manejar Sismos
 * 
 * @author fpainenao
 * 
 */
@RestController
@Api(value = "EarthquakesController")
public class EarthquakesController extends BaseController {

	private static final Logger logger = LoggerFactory.getLogger(EarthquakesController.class);

	@Autowired
	private EarthquakesSismosService earthquakesSismosService;
	
	@Autowired
	private EarthquakesMagnitudService earthquakesMagnitudService;
	
	/**
	 * Metodo que obtiene lista de sismo data una fecha
	 * 
	 * @param request
	 * @return responseEntity<GenericDtoResponse>
	 * @throws ParseException
	 */
	@ApiOperation(value = "Consulta Sismo por rango de Fechas")
	@PostMapping("/consulta/sismo")
	@ApiResponses(value = { @ApiResponse(code = 200, message = " Consulta sismo éxito"),
			@ApiResponse(code = 401, message = "Request no valido") })
	public ResponseEntity<GenericDtoResponse> rangoFecha(@RequestBody EarthquakesRequest request) throws ParseException {
		logger.info(LogMessageUtil.V2_INFO_CONTROLLER_INPUT, InfoEnumUtil.INI_CONSULTA_SISMO_RANGOFECHA.getCode(),
				"/consulta/sismos", LogMessageUtil.BLANK, LogMessageUtil.BLANK, LogMessageUtil.BLANK);

		EarthquakesResponse lDtor = earthquakesSismosService.consultaRangoFecha(request);

		logger.info(LogMessageUtil.V2_INFO_CONTROLLER_OUTPUT, InfoEnumUtil.FIN_CONSULTA_SISMO_RANGOFECHA.getCode(),
				"/consulta/sismos", LogMessageUtil.BLANK, LogMessageUtil.BLANK);

		return this.returnSuccessHttp(lDtor);
	}

	
	
	/**
	 * Metodo que obtiene lista de sismo data una magnitud inicial y final
	 * 
	 * @param request
	 * @return responseEntity<GenericDtoResponse>
	 * @throws ParseException
	 */
	@ApiOperation(value = "Consulta Sismo Magnitud")
	@PostMapping("/consulta/magnitud")
	@ApiResponses(value = { @ApiResponse(code = 200, message = " Consulta magnitud sismo éxito"),
			@ApiResponse(code = 401, message = "Request no valido") })
	public ResponseEntity<GenericDtoResponse> escala(@RequestBody EarthquakesMagnitudRequest request) throws ParseException {
		logger.info(LogMessageUtil.V2_INFO_CONTROLLER_INPUT, InfoEnumUtil.INI_CONSULTA_SISMO_MAGNITUD.getCode(),
				"/consulta/magnitud", LogMessageUtil.BLANK, LogMessageUtil.BLANK, LogMessageUtil.BLANK);

		EarthquakesResponse lDtor = earthquakesMagnitudService.consultaMagnitud(request);

		logger.info(LogMessageUtil.V2_INFO_CONTROLLER_OUTPUT, InfoEnumUtil.FIN_CONSULTA_SISMO_MAGNITUD.getCode(),
				"/consulta/magnitud", LogMessageUtil.BLANK, LogMessageUtil.BLANK);

		return this.returnSuccessHttp(lDtor);
	}
	
	
	/**
	 * Metodo que obtiene lista de sismo data una magnitud inicial y final
	 * 
	 * @param request
	 * @return responseEntity<GenericDtoResponse>
	 * @throws ParseException
	 */
	@ApiOperation(value = "Almacena Sismo Magnitud")
	@PostMapping("/guarda/magnitud")
	@ApiResponses(value = { @ApiResponse(code = 201, message = " Guarda magnitud sismo éxito"),
			@ApiResponse(code = 401, message = "Request no valido") })
	public ResponseEntity<GenericDtoResponse> saveMagnitud(@RequestBody EarthquakesMagnitudRequest request) throws ParseException {
		logger.info(LogMessageUtil.V2_INFO_CONTROLLER_INPUT, InfoEnumUtil.INI_CONSULTA_SISMO_MAGNITUD.getCode(),
				"/consulta/magnitud", LogMessageUtil.BLANK, LogMessageUtil.BLANK, LogMessageUtil.BLANK);

		EarthquakesResponse lDtor = earthquakesMagnitudService.guardaMagnitud(request);

		logger.info(LogMessageUtil.V2_INFO_CONTROLLER_OUTPUT, InfoEnumUtil.FIN_CONSULTA_SISMO_MAGNITUD.getCode(),
				"/consulta/magnitud", LogMessageUtil.BLANK, LogMessageUtil.BLANK);

		return this.returnSuccessHttp(lDtor);
	}

}
