package com.earthquakes.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class AppResponse {

	@JsonProperty("status")
	private Integer status;

	@JsonProperty("message")
	private String message;
		
	@JsonProperty("data")
	@ToString.Exclude 
	private Object data;

	public AppResponse(Integer status, String message, Object data) {
		this.status = status;
		this.message = message;
		this.data = data;
	}

	public AppResponse() {
	}
	
}
