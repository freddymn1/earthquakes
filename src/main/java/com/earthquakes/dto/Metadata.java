package com.earthquakes.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class Metadata {

	
	@JsonProperty("generated")
	private String generated;
	@JsonProperty("url")
	private String url;
	@JsonProperty("title")
	private String title;
	@JsonProperty("status")
	private int status;
	@JsonProperty("api")
	private String api;
	@JsonProperty("count")
	private int count;
  }
