
package com.earthquakes.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "starttime", "endtime"})
@Getter
@Setter
@ToString
public class EarthquakesRequest {

	@JsonProperty("starttime")
	private String startTime;
	@JsonProperty("endtime")
	private String endTime;
	
	
	

}
