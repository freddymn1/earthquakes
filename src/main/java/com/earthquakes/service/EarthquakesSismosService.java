package com.earthquakes.service;

import java.text.ParseException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;

import com.earthquakes.dto.EarthquakesRequest;
import com.earthquakes.dto.EarthquakesResponse;
import com.earthquakes.dto.Sismos;
import com.earthquakes.exception.BusinessException;
import com.earthquakes.utils.Constants;
import com.earthquakes.utils.ErrorEnumUtil;
import com.earthquakes.utils.InfoEnumUtil;
import com.earthquakes.utils.LogMessageUtil;

/**
 * Servicio consulta API Earthquakes
 * 
 * @author fpainenao
 *
 */

@Service
public class EarthquakesSismosService {

	private static final Logger logger = LoggerFactory.getLogger(EarthquakesSismosService.class);

	@Value("${url.service.earthquake}${path.service.earthquake.sismos}")
	private String uriServiceEarthquakeRangoFecha;

	/**
	 * consulta Sismo segun RangoFecha
	 * 
	 * @param
	 * @return
	 * @throws ParseException
	 */
	public EarthquakesResponse consultaRangoFecha(EarthquakesRequest request) throws ParseException {

		EarthquakesResponse response = new EarthquakesResponse();

		logger.info(LogMessageUtil.V2_INFO_SERVICE_INPUT, InfoEnumUtil.INI_CONSULTA_SERVICE.getCode(),
				LogMessageUtil.BLANK);

		ResponseEntity<Sismos> respEarthquakes = sismoRegistrados(request);

		if (respEarthquakes.getStatusCode() == HttpStatus.BAD_REQUEST) {
			throw new BusinessException(ErrorEnumUtil.WSBUK_EARTHQUAKES);
		}
		response.setMessage("Respuesta Exitosa");
		response.setStatus(200);
		response.setData(respEarthquakes.getBody());

		logger.debug(LogMessageUtil.V2_INFO_SERVICE_OUTPUT, InfoEnumUtil.FIN_CONSULTA_SERVICE.getCode(), response);

		return response;
	}

	/**
	 * Servicio Earthquakes
	 *
	 * @param EarthquakesRequest inputEarthquakes)
	 * @return ResponseEntity<Sismos>
	 */

	protected ResponseEntity<Sismos> sismoRegistrados(EarthquakesRequest inputEarthquakes) {

		RestTemplate restTemplate = new RestTemplate();
		String starttime = "&starttime=";
		String endtime = "&endtime=";

		try {

			starttime += inputEarthquakes.getStartTime();
			endtime += inputEarthquakes.getEndTime();

			ResponseEntity<Sismos> respEarthquakes = restTemplate
					.getForEntity(uriServiceEarthquakeRangoFecha + starttime + endtime, Sismos.class);

			if (respEarthquakes.getStatusCodeValue() == Constants.SUCCESS
					&& respEarthquakes.getBody().getMetadata() != null) {

				if (logger.isInfoEnabled()) {
					logger.debug(LogMessageUtil.V2_INFO_SERVICE_OUTPUT,
							InfoEnumUtil.INI_CONSULTA_SERVICE_EARTHQUAKE.getCode(), respEarthquakes.toString());
				}
				return respEarthquakes;
			}

		} catch (HttpStatusCodeException e) {
			logger.error(LogMessageUtil.V2_INFO_SERVICE_OUTPUT, InfoEnumUtil.FIN_CONSULTA_SERVICE_EARTHQUAKE.getCode(),
					e.getStatusCode().value());
			Sismos respEarthquakes = new Sismos();
			return new ResponseEntity<>(respEarthquakes, HttpStatus.BAD_REQUEST);
		}

		return null;

	}

}
