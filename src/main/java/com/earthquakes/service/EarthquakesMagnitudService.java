package com.earthquakes.service;

import java.text.ParseException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;

import com.earthquakes.dto.EarthquakesMagnitudRequest;
import com.earthquakes.dto.EarthquakesRequest;
import com.earthquakes.dto.EarthquakesResponse;
import com.earthquakes.dto.Features;
import com.earthquakes.dto.Sismos;
import com.earthquakes.entity.SismosEntity;
import com.earthquakes.exception.BusinessException;
import com.earthquakes.repository.RegistraSismosRepository;
import com.earthquakes.utils.Constants;
import com.earthquakes.utils.ErrorEnumUtil;
import com.earthquakes.utils.InfoEnumUtil;
import com.earthquakes.utils.LogMessageUtil;

/**
 * Servicio consulta API Earthquakes
 * 
 * @author fpainenao
 *
 */

@Service
public class EarthquakesMagnitudService {

	private static final Logger logger = LoggerFactory.getLogger(EarthquakesMagnitudService.class);

	@Value("${url.service.earthquake}${path.service.earthquake.sismos}")
	private String uriServiceEarthquakeMagnitud;

	@Autowired(required = true)
	RegistraSismosRepository registraSismosRepository;

	/**
	 * consulta Sismo segun Magnitud
	 * 
	 * @param
	 * @return
	 * @throws ParseException
	 */
	public EarthquakesResponse consultaMagnitud(EarthquakesMagnitudRequest request) throws ParseException {

		EarthquakesResponse response = new EarthquakesResponse();

		logger.info(LogMessageUtil.V2_INFO_SERVICE_INPUT, InfoEnumUtil.INI_CONSULTA_SERVICE.getCode(),
				LogMessageUtil.BLANK);

		ResponseEntity<Sismos> respEarthquakes = magnitudSismos(request);

		if (respEarthquakes.getStatusCode() == HttpStatus.BAD_REQUEST) {
			throw new BusinessException(ErrorEnumUtil.WSBUK_EARTHQUAKES);
		}

		response.setMessage("Respuesta Exitosa");
		response.setStatus(200);
		response.setData(respEarthquakes.getBody());

		logger.debug(LogMessageUtil.V2_INFO_SERVICE_OUTPUT, InfoEnumUtil.FIN_CONSULTA_SERVICE.getCode(), response);

		return response;
	}

	/**
	 * Retorno sismo en rango de magnitudes
	 *
	 * @param EarthquakesMagnitudRequest
	 * @return
	 */

	protected ResponseEntity<Sismos> magnitudSismos(EarthquakesMagnitudRequest inputEarthquakes) {

		RestTemplate restTemplate = new RestTemplate();
		String minmagnitude = "&minmagnitude=";
		String maxmagnitude = "&maxmagnitude=";

		try {

			minmagnitude += inputEarthquakes.getMinMagnitude();
			maxmagnitude += inputEarthquakes.getMaxMagnitude();

			ResponseEntity<Sismos> respEarthquakes = restTemplate
					.getForEntity(uriServiceEarthquakeMagnitud + minmagnitude + maxmagnitude, Sismos.class);

			if (respEarthquakes.getStatusCodeValue() == Constants.SUCCESS
					&& respEarthquakes.getBody().getMetadata() != null) {

				if (logger.isInfoEnabled()) {
					logger.debug(LogMessageUtil.V2_INFO_SERVICE_OUTPUT,
							InfoEnumUtil.INI_CONSULTA_SERVICE_EARTHQUAKE.getCode(), respEarthquakes.toString());
				}
				return respEarthquakes;
			}

		} catch (HttpStatusCodeException e) {
			logger.error(LogMessageUtil.V2_INFO_SERVICE_OUTPUT, InfoEnumUtil.FIN_CONSULTA_SERVICE_EARTHQUAKE.getCode(),
					e.getStatusCode().value());
			Sismos respEarthquakes = new Sismos();
			return new ResponseEntity<>(respEarthquakes, HttpStatus.BAD_REQUEST);
		}

		return null;

	}

	/**
	 * registra Sismo segun Magnitud
	 * 
	 * @param
	 * @return
	 * @throws ParseException
	 */
	public EarthquakesResponse guardaMagnitud(EarthquakesMagnitudRequest request) throws ParseException {

		EarthquakesResponse response = new EarthquakesResponse();

		logger.info(LogMessageUtil.V2_INFO_SERVICE_INPUT, InfoEnumUtil.INI_CONSULTA_SERVICE.getCode(),
				LogMessageUtil.BLANK);

		ResponseEntity<Sismos> respEarthquakes = magnitudSismos(request);

		if (respEarthquakes.getStatusCode() == HttpStatus.BAD_REQUEST) {
			throw new BusinessException(ErrorEnumUtil.WSBUK_EARTHQUAKES);
		}

		try {
			List<Features> listaFeatures = respEarthquakes.getBody().getFeatures();
			for (Features features : listaFeatures) {
				SismosEntity sismosEntity = new SismosEntity();

				sismosEntity.setAlert(features.getProperties().getAlert());
				sismosEntity.setCdi(features.getProperties().getCdi());
				sismosEntity.setCode(features.getProperties().getCode());
				sismosEntity.setDetail(features.getProperties().getDetail());
				sismosEntity.setDmin(features.getProperties().getDmin());
				sismosEntity.setFelt(features.getProperties().getFelt());
				sismosEntity.setGap(features.getProperties().getGap());
				sismosEntity.setIds(features.getProperties().getIds());
				sismosEntity.setMag(features.getProperties().getMag());
				sismosEntity.setMagType(features.getProperties().getMagType());
				sismosEntity.setMmi(features.getProperties().getMmi());
				sismosEntity.setNet(features.getProperties().getNet());
				sismosEntity.setNst(features.getProperties().getNst());
				sismosEntity.setPlace(features.getProperties().getPlace());
				sismosEntity.setRms(features.getProperties().getRms());
				sismosEntity.setSig(features.getProperties().getSig());
				sismosEntity.setSources(features.getProperties().getSources());
				sismosEntity.setStatus(features.getProperties().getStatus());
				sismosEntity.setTim(features.getProperties().getTim());
				sismosEntity.setTitle(features.getProperties().getTitle());
				sismosEntity.setTsunami(features.getProperties().getTsunami());
				sismosEntity.setType(features.getProperties().getType());
				sismosEntity.setTypes(features.getProperties().getTypes());
				sismosEntity.setTz(features.getProperties().getTz());
				sismosEntity.setUpdated(features.getProperties().getUpdated());
				sismosEntity.setUrl(features.getProperties().getUrl());

				registraSismosRepository.save(sismosEntity);

			}
		} catch (DataAccessException ex) {
			logger.error(LogMessageUtil.V2_INFO_SERVICE_OUTPUT, InfoEnumUtil.FIN_CONSULTA_SERVICE_EARTHQUAKE.getCode(),
					ex.getLocalizedMessage());

			return null;
		}

		response.setMessage("Respuesta Exitosa");
		response.setStatus(201);
		response.setData(respEarthquakes.getBody());

		logger.debug(LogMessageUtil.V2_INFO_SERVICE_OUTPUT, InfoEnumUtil.FIN_CONSULTA_SERVICE.getCode(), response);

		return response;
	}

}
